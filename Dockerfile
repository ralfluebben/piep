FROM ubuntu:18.04
RUN apt-get -y update && apt-get -y upgrade && apt-get -y install nodejs npm
COPY package.json package.json
RUN npm install
COPY piep.js piep.js
ENV db_addr 127.0.0.1
CMD DB_ADDR=${db_addr} node piep.js

Install Node App: 
```
cd piep
git clone https://gitlab.com/ralfluebben/piep.git
sudo apt update
sudo apt upgrade
sudo apt install nodejs npm
npm install
```

Install MongoDB:
- see https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/

Test:
1. Edit security groups/firewall to allow for port 3000 .
2. Start node server 'DB_ADDR=1.2.3.4 node piep.js'
3. On client:
```
NODE_ADDR=ec2-3-87-203-200.compute-1.amazonaws.com
curl -d '{"username":"myname", "msg":"blablabla"} ' -H "Content-Type:application/json " -X POST http://$NODE_ADDR:3000/piep > result.html
firefox result.html
```

Jmeter Test:
- Get names for testing
```
wget https://raw.githubusercontent.com/fxnn/vornamen/master/Vornamen_Koeln_2017.csv
tail -q -n+2 Vornamen_Koeln_2017.csv Vornamen_Koeln_2017.csv  | cut -f 1 -d ',' | LC_ALL=C grep -v $'[^\t\r -~]' | sort | uniq | tail -q -n 5000  > names.csv
```
- Generate random messages and followers
```
sudo apt install python3-pip
pip3 install Faker
python3 generate_msgs.py > messages.csv
python3 generate_follower.py > follower.csv
```
- Test with JMeter:
Instal plugin ThroughputShapingTimer (https://jmeter-plugins.org/wiki/ThroughputShapingTimer/) first, if not installed already. Note that, test have to be enabled in `./piep_test.jmx`
```
HEAP="-Xms8g -Xmx8g -XX:MaxMetaspaceSize=2g" ~/apacha-jmeter-5.5/bin/jmeter -n -J jmeter.reportgenerator.overall_granularity=1000 -t ./piep_test.jmx -l log.jlt -e -o results
```

# Docker:
Install docker and use:
```
docker build .
docker run -p 3001:3000 -e "db_addr=172.31.33.189" -d d096d3ed0a45
```

# Kubernetes

Deploy a mongodb outside of the cluster.

Set the DB Addr in the deployment file:
```
        env:
        - name: db_addr
          value: "172.23.1.28"
```

Deploy the redis cache as stateful set, the related service, and set master/slave roles:
```
kubectl apply -f redis-sts.yaml
kubectl apply -f redis-svc.yaml
# get IP addresses of the redis cluster
kubectl get pods -l app=redis-cluster -o jsonpath='{range.items[*]}{.status.podIP}:6379 '
# add all nodes to the cluster
kubectl exec -it redis-cluster-0 -- redis-cli --cluster create --cluster-replicas 1 <ipaddr1:6379> <ipaddr2:6379> ...
```
Simplified from https://rancher.com/blog/2019/deploying-redis-cluster/, without persistent volumes.


Deploy the APP and start the service:
```
kubectl apply -f piep_redis-deployment.yaml
kubectl apply -f piep_redis-service.yaml
```
Deploy the ingress service as bare metal (if not done already)
- https://kubernetes.github.io/ingress-nginx/deploy
- https://kubernetes.github.io/ingress-nginx/deploy/baremetal/#over-a-nodeport-service

and apply the configuration for the ingress to link to the service
```
kubectl apply -f ingress.yaml
```
Note that the bare metal configuration of Ingress creates a NodePort Service. Get the port with
```
ubuntu@master:~/piep$ kubectl get services -n ingress-nginx
NAME                                 TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)                      AGE
ingress-nginx-controller             NodePort    10.108.254.211   <none>        80:32439/TCP,443:31574/TCP   20m
ingress-nginx-controller-admission   ClusterIP   10.99.25.136     <none>        443/TCP                      20m
```
You can access the service via the port 32439 in this example.

Test with `curl`:
```
HOST="172.23.1.29:30050"

curl -d '{"username":"ralf", "msg":"hallo"}' -H "Content-type:application/json" -X POST http://${HOST}/piep

curl -d '{"username":"bella", "msg":"wuff"}' -H "Content-type:application/json" -X POST http://${HOST}/piep

curl http://${HOST}/users

curl "http://${HOST}/follow?me=ralf&follow=bella"

curl "http://${HOST}/feedme/ralf

```




import csv
from faker import Faker
fake = Faker()
with open('names.csv') as csv_file:
    csv_reader = csv.reader(csv_file)
    for row in csv_reader:
        if len(row) > 0:
            for c in range(0, 24):
                print(row[0] + ',"' + fake.text().replace('\n', ' ')+'"')

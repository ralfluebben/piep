var express = require('express');
var qr = require('qr-image');
const crypto = require('crypto');
var mongo = require('mongodb');
var app = express();
var async = require("async");
var sizeof = require('object-sizeof');

var MongoClient = require('mongodb').MongoClient;
const db_addr = process.env.DB_ADDR
const url = "mongodb://"+ db_addr + "/?compressors=snappy,zlib";
console.log("DB: " + url);
app.use(express.json())

var Redis = require("ioredis");

var cluster = new Redis.Cluster([
  {
    port: 6379,
    host: "redis-cluster",
    showFriendlyErrorStack: true,
    scaleReads: "all"
  }
]);
/*
var cluster = new Redis(6379, "mc1.wi.fh-flensburg.de");
*/
app.get('/', function (req, res) {
	console.log("Request from: " + req.ip)
	res.send("Hello World!");
});


app.post('/piep',function (req, res) {
  // Check if username is empty and only alphanumeric, max length 30
  if (!req.body.username) {
    console.log('Username not defined');
    res.sendStatus(400);
    return;
  }
  if (req.body.username.length > 30) {
    console.log('Username exceed length');
    res.sendStatus(400);
    return;
  }
  if (req.body.username.length === 0 || !req.body.username.trim()) {
    console.log('Username empty');
    res.sendStatus(400);
    return;
  }
  // Check if message is not empty and only alphanumeric, max length 1000
  if (!req.body.msg) {
    console.log('msg not defined');
    res.sendStatus(400);
    return;
  }
  if (req.body.msg.length > 1000) {
    console.log('msg exceed length');
    res.sendStatus(400);
    return;
  }
  if (req.body.msg.length === 0 || !req.body.msg.trim()) {
    console.log('msg empty');
    res.sendStatus(400);
    return;
  }
  // create QR code from message (base64)
  var qr_png = qr.imageSync(req.body.msg,{ type: 'png'});
  
  // create QR avatar from message (base64)
  const Avatar = require('avatar-builder');
  const avatar = Avatar.catBuilder(128);

  var hash = crypto.createHash("sha512").update(req.body.username, "binary").digest("hex");
  var ab = avatar.create(hash).then(buffer =>  {

    var MongoClient = require('mongodb').MongoClient;
    const buf6 = Buffer.from(buffer);

    MongoClient.connect(url, {useUnifiedTopology: true, useNewUrlParser: true}, function(err, db) {
      if (err) {
        console.log("Connection error");
        console.log(err.toString());
      } else {
        var dbo = db.db("piep");
        var myobj = { username: req.body.username, message : req.body.msg, code: qr_png.toString('base64'), avatar: buf6.toString('base64') };
        dbo.collection("messages").insertOne(myobj, function(err, result) {
          if (err) {
            console.log("Insertion error");
            console.log(err.toString());
          } else {
            res.write('<html><head></head><body>');
            res.write('<table style="width:100%">')
            res.write('<tr> <th>Username</th> <th>Msg</th> <th>Code</th> <th>Avatar</th> </tr>')
            res.write('<tr> <th>' + req.body.username + '</th> <th>' + req.body.msg + '</th> <th> <img src="data:image/png;base64,' + qr_png.toString('base64') + '" alt="QR Code"> </th> <th> <img src="data:image/png;base64,' + buf6.toString('base64') + ' " alt="Avatar"></th> </tr>')
            res.end('</table></body></html>');
            console.log(result.ops[0]);
            db.close();
          }
        });
      }
    }); 
  });
});

app.get('/users',function (req, res) {
  console.log('Get /users');
  var MongoClient = require('mongodb').MongoClient;

  MongoClient.connect(url, {useUnifiedTopology: true, useNewUrlParser: true}, function(err, db) {
    if (err) {
      console.log("Connection error");
      console.log(err.toString());
    } else {
      var dbo = db.db("piep");
      dbo.collection("messages").distinct('username',{}, function(err, result) {
        if (err) throw err;
        res.write(result.toString());
        res.end('');
        console.log(result);
        db.close();
      });
    }
  });
});

app.get('/follow', (req, res) => {
  if ((!req.query.me) || (!req.query.follow))
  {
    res.sendStatus(400);
    return;
  }
  var MongoClient = require('mongodb').MongoClient;

    MongoClient.connect(url, {useUnifiedTopology: true, useNewUrlParser: true}, function(err, db) {
      if (err) {
        console.log("Connection error");
        console.log(err.toString());
      } else {
        var dbo = db.db("piep");
        dbo.collection("following").updateOne( 
          {'username': req.query.me }, 
          { $addToSet: { 'following': req.query.follow } },
          { upsert: true}, function(err, result) {
            if (err) throw err;
            console.log(result.result);
            if (err) {
              console.log("upsert error");
              console.log(err.toString());
              res.sendStatus(500);
            } else {
              console.log(req.query.me);
              console.log(req.query.follow);
              res.sendStatus(200);
            }
          db.close();
        });    
      }
    }); 
})

app.get('/feedme/:name', (req, res) => {
  var MongoClient = require('mongodb').MongoClient;

  MongoClient.connect(url, {useUnifiedTopology: true, useNewUrlParser: true}, function(err, db) {
    if (err) {
      console.log("Connection error");
      console.log(err.toString());
    } else {
      var dbo = db.db("piep");
      async.waterfall([
        //get following
        function (callback) {
          console.log("Get following ...");
          dbo.collection("following").find(
            {'username': req.params.name}, { projection: { following: 1 } }).toArray(function(err, result) {
                  if (err) throw err;
                  if (result.length) {
                    callback(null, result[0].following);
                  } else {
                    callback(null, []);
                  }
              });
        }, function( users, callback) {
            var nof_keys=users.length;
            res.write('<html><head></head><body>');
            res.write('<table style="width:100%">')
            res.write('<tr> <th>Username</th> <th>Msg</th> <th>Code</th> <th>Avatar</th> </tr>')   
            users_cache = [];
            users_db =[];
            if (nof_keys == 0) {
              callback(null, users );
            }
            for(let u of users) {
              cluster.lrange(u, 0, -1, function(err, msgs){
                if (typeof msgs != 'undefined')
                {
                  var c=0;
                  for(let ms of msgs)
                  {
                    m = JSON.parse(ms);
                    res.write('<tr> <td>' + m.username + '</td> <td style="word-break:break-word">' + m.message + '</td> <td> <img src="data:image/png;base64,' + m.code + '" alt="QR Code"> </td> <td> <img src="data:image/png;base64,' + m.avatar + ' " alt="Avatar"></td> </tr>')
                    c=c+1;
                  }
                  if (c==0) 
                    users_db.push(u);
                  else 
                    //refresh cache
                    cluster.expire(u,1000);
                } else {
                  users_db.push(u);
                }
                nof_keys=nof_keys-1;
                console.log(nof_keys)
                if (nof_keys == 0) callback(null, users_db);
              });
            }
          }, function( users_db, callback) {
            console.log("\tGet following from db... "+ + users_db.length);
            if (users_db.length == 0)
            {
              callback(null);
            } else {
              dbo.collection("messages").find({'username': { $in: users_db}}).toArray(function(err, result2) {
                if (err) throw err;
                var nof_keys=result2.length;
                if (nof_keys == 0) 
                  callback(null);
                var user_msgs = {};
                for(let val of result2){
                  res.write('<tr> <td>' + val.username + '</td> <td style="word-break:break-word">' + val.message + '</td> <td> <img src="data:image/png;base64,' + val.code + '" alt="QR Code"> </td> <td> <img src="data:image/png;base64,' + val.avatar + ' " alt="Avatar"></td> </tr>')

                  if (val.username in user_msgs)
                    user_msgs[val.username].push(JSON.stringify(val))
                  else
                    user_msgs[val.username]=[JSON.stringify(val)]
                  nof_keys=nof_keys-1;
                  if (nof_keys == 0) callback(null);
                }
                // push none cached users to redis cache
                for(var key in user_msgs) {
                  cluster.lpush(key, ...user_msgs[key]);
                  //chache for 1000s
                  cluster.expire(key,1000)
                }
                db.close();
              });
            } 
          }
            ], function (err) {
          res.end('</table></body></html>');
          console.log("\tDone")
      });
    }
  });
})

app.listen(3000, "0.0.0.0", function () {
  console.log('Example app listening on port 3000!');
});


